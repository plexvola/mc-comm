#include <sys/socket.h>
#include "mctypes.h"

#define PING IPPROTO_TCP
#define QUERY IPPROTO_UDP

void connect_mc_server(int socket, char* address, unsigned short port, int protocol);
void send_ping_handshake(int socket, int protocol, char* address, unsigned short port, int next_state);
void send_ping_packet(int socket, int packet_id, size_t raw_size, void* raw_data);
packet recv_packet(int socket);
varint recv_varint(int socket);
