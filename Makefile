CC=clang
CFLAGS=-Wall -g -I./include
PREFIX=~/.local

mcping: mcping.c mctypes.o mcnetwork.o
	$(CC) $(CFLAGS) $^ -o $@

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $<

test: mcping
	./mcping 137.74.234.16:2001 | jq '.players'
	./mcping mc.seisan.fr | jq '.players'

lint:
	indent -kr -ts4 *.c

clean:
	rm -f *.o *~

install: mcping
	cp $^ $(PREFIX)/bin
